﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit.Sdk;

namespace JsTester.Xunit
{
    public class PhantomjsTestClassCommand: ITestClassCommand
    {
        private readonly TestClassCommand _command = new TestClassCommand();
        private readonly Func<Type, IPhantomjsTestRunner> _runnerFactory;

        public PhantomjsTestClassCommand()
            : this(t => new JasmineTestRunner())
        {
        }

        public PhantomjsTestClassCommand(Func<Type, IPhantomjsTestRunner> runnerFactory)
        {
            _runnerFactory = runnerFactory;
        }

        public object ObjectUnderTest
        {
            get { return _command.ObjectUnderTest; }
        }

        public ITypeInfo TypeUnderTest
        {
            get { return _command.TypeUnderTest; }
            set { _command.TypeUnderTest = value; }
        }

        public int ChooseNextTest(ICollection<IMethodInfo> testsLeftToRun)
        {
            return _command.ChooseNextTest(testsLeftToRun);
        }

        public Exception ClassFinish()
        {
            return _command.ClassFinish();
        }

        public Exception ClassStart()
        {
            return _command.ClassStart();
        }

        public IEnumerable<ITestCommand> EnumerateTestCommands(IMethodInfo testMethod)
        {
            try
            {
                if (testMethod.HasAttribute(typeof(JasmineSpecAttribute)))
                {
                    var specAttr =
                        testMethod.GetCustomAttributes(typeof(JasmineSpecAttribute)).Single().GetInstance
                            <JasmineSpecAttribute>();
                    var spec = String.IsNullOrEmpty(specAttr.SpecRunnerFile) ? testMethod.Name + ".html" : specAttr.SpecRunnerFile;

                    var runner = _runnerFactory(typeof(JasmineSpecAttribute));
                    runner.Initialize();
                    return runner.RunTests(spec, specAttr.Timeout).Select(r => new PhantomjsTestCommand(testMethod, r));
                }
                
                return _command.EnumerateTestCommands(testMethod);
            }
            catch (Exception e)
            {
                var failResult = JsResult.Fail(testMethod.Name, e);

                if (e is System.ComponentModel.Win32Exception)
                {
                    // Workaround for exception thrown when phantomjs.exe is not found. 
                    //  The exception does not contain the name of the file!
                    failResult.ErrorMessage += String.Format(" (file: {0})", PhantomjsProcessExecuter.PHANTOMJS_EXE);
                }
               
                return new List<ITestCommand>()
                           {
                               new PhantomjsTestCommand(testMethod, failResult)
                           };
            }   
        }

        public IEnumerable<IMethodInfo> EnumerateTestMethods()
        {
            return _command.EnumerateTestMethods();
        }

        public bool IsTestMethod(IMethodInfo testMethod)
        {
            return _command.IsTestMethod(testMethod);
        }
    }
}
