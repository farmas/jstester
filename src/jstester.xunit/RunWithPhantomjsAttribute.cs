﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace JsTester.Xunit
{
    public class RunWithPhantomjsAttribute: RunWithAttribute
    {
        public RunWithPhantomjsAttribute()
            : base(typeof(PhantomjsTestClassCommand))
        {
        }
    }
}
