﻿using System;
using System.Collections.Generic;
namespace JsTester
{
    public interface IPhantomjsTestRunner
    {
        IEnumerable<JsResult> RunTests(string args, int timeout);
        void Initialize();
    }
}
