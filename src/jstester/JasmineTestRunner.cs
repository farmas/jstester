using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace JsTester
{
    public class JasmineTestRunner : IPhantomjsTestRunner
    {
        public const string PHANTOMJS_USER_AGENT = "PhantomJS";
        public const string JASMINE_RUNNER = "phantomjs-run-spec.js";
        public const string JASMINE_REPORTER = "jasmine-console-reporter.js";
        public const string KEY_PASS = "[PASS]";
        public const string KEY_FAIL = "[FAIL]";
        public const string KEY_ERROR = "[ERROR]";

        private readonly IFileSystem _fileSystem;
        private readonly IProcessExecuter _phantomjsRunner;
        private readonly string _workingDir;

        public JasmineTestRunner(string workingDir = null)
            : this(workingDir, new FileSystem(), new PhantomjsProcessExecuter())
        {
        }

        public JasmineTestRunner(string workingDir, IFileSystem fileSystem, IProcessExecuter phantomjsRunner)
        {
            _phantomjsRunner = phantomjsRunner;
            _fileSystem = fileSystem;
            _workingDir = workingDir ?? Path.GetDirectoryName(typeof(PhantomjsProcessExecuter).Assembly.Location);
        }

        public void Initialize()
        {
            EnsureResourceExists(JASMINE_REPORTER);
            EnsureResourceExists(JASMINE_RUNNER);    
        }

        public IEnumerable<JsResult> RunTests(string specFile, int timeout)
        {
            Preconditions.CheckNotNull(specFile, "specFile");

            var specFullPath = Path.Combine(_workingDir, specFile);

            Preconditions.Check(_fileSystem.Exists(specFullPath), 
                () => new FileNotFoundException(
                    String.Format("Spec runner file '{0}' was not found.", specFullPath), specFullPath));

            CheckSpecHasPhantomUserAgent(specFullPath);

            var lines = _phantomjsRunner.Execute(String.Format("{0} {1}", JASMINE_RUNNER, specFile), timeout);
            return ParseOutput(lines, specFile + ": ");
            
        }

        private void CheckSpecHasPhantomUserAgent(string specFullPath)
        {
            Preconditions.Check(_fileSystem.ReadAllText(specFullPath).Contains(PHANTOMJS_USER_AGENT),
                String.Format("The spec runner file '{0}' is missing a check for '{1}' user agent. " +
                    "Spec runner file should skip executing jasmine tests on page load when running with PhantomJS. " +
                    @"Make sure to add the following check: 
if (navigator.userAgent.indexOf('PhantomJS') == -1) {{
    jasmine.getEnv().execute();
}}", specFullPath, PHANTOMJS_USER_AGENT));
        }

        public IEnumerable<JsResult> ParseOutput(IEnumerable<string> output, string prefix = "")
        {
            var results = new List<JsResult>();

            foreach (var l in output)
            {
                if (l.StartsWith(KEY_PASS))
                {
                    results.Add(JsResult.Pass(prefix + ExtractLineMessage(l)));
                }
                else if (l.StartsWith(KEY_FAIL))
                {
                    results.Add(JsResult.Fail(prefix + ExtractLineMessage(l)));
                }
                else if (l.StartsWith(KEY_ERROR))
                {
                    var last = results.LastOrDefault();
                    if (last != null && last.ErrorMessage == JsResult.UNKNOWN_ERROR)
                    {
                        last.ErrorMessage = ExtractLineMessage(l);
                    }
                }
            }
            return results;
        }

        private static string ExtractLineMessage(string line)
        {
            return line.Substring(line.IndexOf("]") + 1).Trim();
        }

        private void EnsureResourceExists(string fileName)
        {
            var filePath = Path.Combine(_workingDir, fileName);
            if (!_fileSystem.Exists(filePath))
            {
                using (var stream = typeof(PhantomjsProcessExecuter).Assembly
                    .GetManifestResourceStream("JsTester.resources." + fileName))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        _fileSystem.WriteAllText(filePath, reader.ReadToEnd());
                    }
                }
            }
        }
    }
}