﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsTester
{
    public class JsResult
    {
        public const string UNKNOWN_ERROR = "[Unspecified Error]";

        public string Name { get; set; }
        public bool Passed { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }

        private JsResult(string name, bool passed,
            string errorType = null, string errorMessage = null)
        {
            Preconditions.CheckNotNull(name, "name");
            
            Name = name;
            Passed = passed;
            ErrorType = errorType;
            ErrorMessage = errorMessage;
        }

        public static JsResult Pass(string name)
        {
            return new JsResult(name, true);
        }

        public static JsResult Fail(string name)
        {
            return new JsResult(name, false, UNKNOWN_ERROR, UNKNOWN_ERROR);
        }

        public static JsResult Fail(string name, Exception e)    
        {
            Preconditions.CheckNotNull(e, "exception");

            return new JsResult(
                name,
                false,
                e.GetType().FullName,
                e.ToString());
        }
    }
}
