﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JsTester.Xunit;

namespace Tests.Integration
{
    [RunWithPhantomjs]
    public class JasmineTests
    {
        [JasmineSpec(SpecRunnerFile="JasmineTests_AllPassing.html")]
        public void AllPassing()
        {
        }

        [JasmineSpec]
        public void JasmineTests_OneFailing()
        {
        }
    }
}