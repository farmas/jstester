﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit.Sdk;
using JsTester.Xunit;

namespace Test.JsTester.Xunit
{
    public static class MockExtensions
    {
        public static void AddJasmineAttribute(this Mock<IMethodInfo> methodInfo, JasmineSpecAttribute a)
        {
            methodInfo.Setup(m => m.HasAttribute(a.GetType())).Returns(true);

            var info = new Mock<IAttributeInfo>();
            info.Setup(i => i.GetInstance<JasmineSpecAttribute>()).Returns(a);

            methodInfo.Setup(m => m.GetCustomAttributes(a.GetType())).Returns(new IAttributeInfo[]{
                info.Object
            });
        }
    }
}
