﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Moq;
using Xunit.Sdk;
using JsTester;
using JsTester.Xunit;

namespace Test.JsTester.Xunit
{
    public class PhantomjsTestClassCommandTests
    {
        [Fact]
        public void calls_initialize_on_the_runner()
        {
            var jasmineAttr = new JasmineSpecAttribute() { SpecRunnerFile = "TestRunner.html" };
            var methodInfo = new Mock<IMethodInfo>();
            methodInfo.AddJasmineAttribute(jasmineAttr);

            var testClassCommand = TestableClassCommand.Create();
            testClassCommand.EnumerateTestCommands(methodInfo.Object);

            testClassCommand.Runner.Verify(r => r.Initialize());
        }

        [Fact]
        public void use_value_from_attribute_as_spec_name()
        {
            var jasmineAttr = new JasmineSpecAttribute() { SpecRunnerFile = "TestRunner.html" };
            var methodInfo = new Mock<IMethodInfo>();
            methodInfo.AddJasmineAttribute(jasmineAttr);

            var testClassCommand = TestableClassCommand.Create();
            testClassCommand.EnumerateTestCommands(methodInfo.Object);

            testClassCommand.Runner.Verify(r => r.RunTests("TestRunner.html", It.IsAny<int>()));
        }

        [Fact]
        public void use_method_name_as_spec_name_if_not_specified_in_attribute()
        {
            var jasmineAttr = new JasmineSpecAttribute();
            var methodInfo = new Mock<IMethodInfo>();
            methodInfo.AddJasmineAttribute(jasmineAttr);
            methodInfo.SetupGet(m => m.Name).Returns("MySpecName");

            var testClassCommand = TestableClassCommand.Create();
            testClassCommand.EnumerateTestCommands(methodInfo.Object);

            testClassCommand.Runner.Verify(r => r.RunTests("MySpecName.html", It.IsAny<int>()));
        }

        public class TestableClassCommand : PhantomjsTestClassCommand
        {
            public Mock<IPhantomjsTestRunner> Runner { get; set; }

            private TestableClassCommand(Mock<IPhantomjsTestRunner> runner)
                : base(t => runner.Object)
            {
                Runner = runner;
            }

            public static TestableClassCommand Create()
            {
                return new TestableClassCommand(new Mock<IPhantomjsTestRunner>());
            }
        }
    }
}
