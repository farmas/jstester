﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Moq;
using Xunit.Sdk;
using JsTester;
using JsTester.Xunit;

namespace Test.JsTester.Xunit
{
    class PhantomjsTestCommandTests
    {
        [Fact]
        public void execute_returns_passed_result()
        {
            var methodInfo = new Mock<IMethodInfo>();         
            var command = new PhantomjsTestCommand(methodInfo.Object, JsResult.Pass("pass message"));

            var result = command.Execute(null);

            Assert.IsType(typeof(PassedResult), result);
            Assert.Equal("pass message", result.DisplayName);
        }

        [Fact]
        public void execute_returns_failed_result()
        {
            var methodInfo = new Mock<IMethodInfo>();
            var command = new PhantomjsTestCommand(methodInfo.Object, JsResult.Fail("fail message"));

            var result = command.Execute(null);

            Assert.IsType(typeof(FailedResult), result);
            Assert.Equal("fail message", result.DisplayName);
        }
    }
}
