﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit.Sdk;
using JsTester;

namespace Test.JsTester
{
    public static class MockExtensions
    {
        public static void AddFile(this Mock<IFileSystem> fs, string path, string contents = "PhantomJS")
        {
            fs.Setup(f => f.Exists(path)).Returns(true);
            if (contents != null)
            {
                fs.Setup(f => f.ReadAllText(path)).Returns(contents);
            }
        }
    }
}
